from datetime import timedelta

import app
import airflow
from airflow import DAG
from airflow.operators.from airflow.operators.python_operator import PythonOperator


default_args = {
    "id" : "toto",
    "owner" : "moi",
    'start_date': airflow.utils.dates.days_ago(2),
    "schedule_interval" : "daily"
}

dag = DAG(
    'toto', 
    default_args=default_args,
    schedule_interval=timedelta(days=1),
)

get_data = PythonOperator(
    task_id="get_data",
    python_callable=get_data,
    dag=dag
)