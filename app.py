import requests
import pymongo
import datetime
import json

def get_data():
    data = {
    'profile': requests.api.get('https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=3344c11251f977cf3d86c5857e8c7895').json(),
    'rating' : requests.api.get('https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=3344c11251f977cf3d86c5857e8c7895').json(),
    'timestamp' : datetime.datetime.now().timestamp()
    }
    print(data)
    with open('/tmp/data.json','w') as f:
        json.dump(data,f)

def insert_data():

    myclient = pymongo.MongoClient("mongodb://root:password@localhost:27017/")
    mydb = myclient["db"]
    mycol = mydb["col"]

    with open('/tmp/data.json','r') as f:
        data = json.load()
        x = mycol.insert_one(data)
